﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW_47.Models
{
    public class Car
    {
        public int Id { get; set; }
        public string ModelName { get; set; }
        public string MakeName { get; set; }
        public string VinCode { get; set; }
        public int Year { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
