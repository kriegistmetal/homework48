﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW_47.Models
{
    public class CarsContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Car> Cars { get; set; }

        public CarsContext(DbContextOptions<CarsContext> options)
            : base(options)
        {
        }
    }
}